var fs = require('fs');


// Reading data from file
var data = fs.readFileSync('test.txt', 'utf8');
console.log(data);

// creating and writing to a file
fs.writeFileSync('writeToFile.txt', "Welcome to Tech Brothers tutorials "+data);
